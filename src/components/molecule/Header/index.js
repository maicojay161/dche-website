import React, { useState } from "react";
import { Link } from "react-router-dom";
import { SearchIcon } from "@icons"

import CloseIcon from '@mui/icons-material/Close';
import MenuIcon from '@mui/icons-material/Menu';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

import Logo1 from "@assets/img/logo/DOCE-Logo2.png"
import Logo2 from "@assets/img/logo/DOCE-Logo3.png"
import Logo3 from "@assets/img/logo/DOCE-Logo1.png"
import Logo4 from "@assets/img/logo/DOCE-Title.png"
import style from "@molecule/Header/style.scss"

const openNav = () => (
    document.getElementById("sideNav").style.width = "100%"
)

const closeNav = () => (
    document.getElementById("sideNav").style.width = "0"
)

export default function Header() {

    const [openAbout, setOpenAbout] = useState(false);
    const [openOurPeople, setOpenOurPeople] = useState(false);
    const [openPrograms, setOpenPrograms] = useState(false);
    const [openResearch, setOpenResearch] = useState(false);
    const [openNE, setOpenNE] = useState(false)

    return (
       <div>
            <div className="row">
                <div className="nav-upper-color-1"></div>
                <div className="nav-upper-color-2"></div>
                <div className="nav-upper-color-3"></div>
                <div className="nav-upper-color-4"></div>
                <div className="nav-upper-color-5"></div>
                <div className="nav-upper-color-6"></div>
            </div>
           <div className="header-nav">
                <div className="wrapper">
                    <div className="upper-nav">
                        <div className="main-logo">
                            <img src={Logo1} className="uplogo"/>
                            <img src={Logo2} className="englogo"/>
                            <img src={Logo3} className="dchelogo"/>
                            <img src={Logo4} className="titlelogo"/>
                        </div>
                        <Link to={'#'} className="menu hide-on-med-and-large" onClick={openNav}><MenuIcon/></Link>
                        {/* <div className="search hide-on-med-and-down">
                           <div className="search-container">
                                <SearchIcon />
                                <input type="text" placeholder="Search"/>
                           </div>
                        </div> */}
                    </div>
                </div>
                <div className="nav-links hide-on-med-and-down">
                    <div className="container">
                        <ul>
                            <li><Link to={"/"}>Home</Link></li>
                            <li id="about-us">
                                <Link>About Us</Link>
                                <ul className="dropdown">
                                    <li><Link to={"/deanswelcome"}>Chair's Message</Link></li>
                                    <li><Link to={"/visionandmission"}>Vision and Mission</Link></li>
                                    <li><Link to={"/governingboard"}>ADMINISTRATION</Link></li>
                                </ul>
                            </li>
                            <li id="our-people">
                                <Link>Our People</Link>
                                <ul className="dropdown">
                                    <li><Link to={"/faculty"}>Faculty</Link></li>
                                    <li><Link to={"/students"}>Students</Link></li>
                                    <li><Link to={"/alumni"}>Alumni</Link></li>
                                    {/* <li><Link to={"/alumni"}>Alumni</Link></li>
                                    <li><Link to={"/staff"}>Staffs</Link></li> */}
                                </ul>
                            </li>
                            <li id="graduates">
                                <Link>Programs</Link>
                                <ul className="dropdown">
                                    <li><Link to={"/programsBSCHE"}>BS ChE</Link></li>
                                    <li><Link to={"/programsMSCHE"}>MS ChE</Link></li>
                                    <li><Link to={"/programsPHDCHE"}>PhD ChE</Link></li>
                                </ul>
                            </li>
                            <li id="graduates">
                                <Link>Research</Link>
                                <ul className="dropdown">
                                    <li><Link to={"/researchareas"}>Research Areas</Link></li>
                                    <li><Link to={"/researchlab"}>Research Laboratories</Link></li>
                                    <li><Link to={"/publications"}>Publications</Link></li>
                                </ul>
                            </li>
                            {/* <li><Link to={"/publications"}>Publications</Link></li> */}
                            <li><Link to={"/newsandevents"}>News And Events</Link></li>
                            {/* <li id="newsandevents">
                                <Link>News And Events</Link>
                                <ul className="dropdown">
                                    <li><Link to={"/news"}>News</Link></li>
                                </ul>
                            </li> */}
                            <li><Link to={"/services"}>Services</Link></li>
                            <li><Link to={"/contactus"}>Contact Us</Link></li>
                            {/* <li><Link to={"/dcheat70"}>DCHE at 70</Link></li> */}
                        </ul>
                    </div>
                </div>

                <div id="sideNav" className="sidenav">
                    <Link className="closebtn" onClick={closeNav}><CloseIcon/></Link>
                    <ul className="main-ul">

                        <li className="main-ul"><Link to={"/"} onClick={closeNav}>Home</Link></li>

                        <li className="main-ul">
                            <Link to={"#"} onClick={()=> openAbout === true ? setOpenAbout(false) : setOpenAbout(true)}>About Us <ArrowDropDownIcon/></Link>
                            <ul className={ openAbout === true ? 'dropdown openDrop' : 'dropdown closeDrop'}>
                                <li><Link to={"/deanswelcome"} onClick={closeNav}>Chair's Message</Link></li>
                                <li><Link to={"/visionandmission"} onClick={closeNav}>Vision and Mission</Link></li>
                                <li><Link to={"/governingboard"} onClick={closeNav}>Administration</Link></li>
                            </ul>
                        </li>

                        <li className="main-ul">
                            <Link to={"#"} onClick={()=> openOurPeople === true ? setOpenOurPeople(false) : setOpenOurPeople(true)}>Our People<ArrowDropDownIcon/></Link>
                            <ul className={ openOurPeople === true ? 'dropdown openDrop' : 'dropdown closeDrop'}>
                                <li><Link to={"/faculty"} onClick={closeNav}>Faculty</Link></li>
                                <li><Link to={"/students"} onClick={closeNav}>Students</Link></li>
                            </ul>
                        </li>

                        <li className="main-ul">
                            <Link to={"#"} onClick={()=> openPrograms === true ? setOpenPrograms(false) : setOpenPrograms(true)}>Programs<ArrowDropDownIcon/></Link>
                            <ul className={ openPrograms === true ? 'dropdown openDrop' : 'dropdown closeDrop'}>
                                <li><Link to={"/programsBSCHE"} onClick={closeNav}>BS ChE</Link></li>
                                <li><Link to={"/programsMSCHE"} onClick={closeNav}>MS ChE</Link></li>
                                <li><Link to={"/programsPHDCHE"} onClick={closeNav}>PhD ChE</Link></li>
                            </ul>
                        </li>

                        <li className="main-ul">
                            <Link to={"#"} onClick={()=> openResearch === true ? setOpenResearch(false) : setOpenResearch(true)}>Research<ArrowDropDownIcon/></Link>
                            <ul className={ openResearch === true ? 'dropdown openDrop' : 'dropdown closeDrop'}>
                                <li><Link to={"/researchareas"} onClick={closeNav}>Research Areas</Link></li>
                                <li><Link to={"/researchlab"} onClick={closeNav}>Research Laboratories</Link></li>
                                <li><Link to={"/publications"} onClick={closeNav}>Publications</Link></li>
                            </ul>
                        </li>

                        {/* <li className="main-ul"><Link to={"/publications"}>Publications</Link></li> */}
                        <li className="main-ul"><Link to={"/newsandevents"} onClick={closeNav}>News And Events</Link></li>

                        {/* <li className="main-ul">
                            <Link to={"#"} onClick={()=> openNE === true ? setOpenNE(false) : setOpenNE(true)}>News And Events<ArrowDropDownIcon/></Link>
                            <ul className={ openNE === true ? 'dropdown openDrop' : 'dropdown closeDrop'}>
                                <li><Link to={"/news"}>News</Link></li>c
                            </ul>
                        </li> */}

                        <li className="main-ul"><Link to={"/services"} onClick={closeNav}>Services</Link></li>
                        <li className="main-ul"><Link to={"/contactus"} onClick={closeNav}>Contact Us</Link></li>

                        {/* <li className="main-ul"><Link to={"/dcheat70"}>DCHE at 70</Link></li> */}

                    </ul>
                </div>
           </div>
       </div>
    );
}