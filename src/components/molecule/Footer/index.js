import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Logo from "@assets/img/logo.png"
import Logo1 from "@assets/img/logo/DOCE-Logo2.png"
import Logo2 from "@assets/img/logo/DOCE-Logo3.png"
import Logo3 from "@assets/img/logo/DOCE-Logo1.png"
import Logo4 from "@assets/img/logo/DOCE-Title.png"
import {  PhoneForwardedIcon, FacebookIcon, InstagramIcon, EmailIcon, TwitterIcon} from "@icons"
import axios from 'axios'
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import style from "@molecule/Footer/style.scss"


export default function Footer() {

    const [contactUs, setContactUs] = useState([])

    useEffect(() => {
        axios.get(`/contact-us-details`)
        .then((response) => {
            setContactUs(response.data.data.contactUsDetails)
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    return (
       <div>
           <div className="footer-section">
                <div className="upper-footer ">
                    <div className="wrapper">
                        <div className="upper">
                            <h1>Connect With Us</h1>
                            {/* <div className="social-media">
                                <FacebookIcon /> <InstagramIcon /> <EmailIcon /> <TwitterIcon/>
                            </div> */}
                        </div>
                        <div className="mid">
                            <div className="main-logo">
                                <img src={Logo1} className="uplogo"/>
                                <img src={Logo2} className="englogo"/>
                                <img src={Logo3} className="dchelogo"/>
                                <img src={Logo4} className="titlelogo"/>
                            </div>
                        </div>

                        {
                            contactUs.length != 0 ? 
                            contactUs.map((data,index) => {
                                return(
                                    <div className="lower hide-on-med-and-down">
                                        <div className="address">
                                            <p className="address-title">Department of Chemical Engineering</p>
                                            <p className="address-details">
                                            {ReactHtmlParser(data.address)}
                                            </p>
                                        </div>
                                        <div className="telephone">
                                            <p className="telephone-title"> <PhoneForwardedIcon/> Telephone:</p>
                                            <p className="telephone-details">
                                            {ReactHtmlParser(data.phone)}
                                            </p>
                                        </div>
                                        <div className="email">
                                            <p className="email-title"> <FacebookIcon /> Facebook:</p>
                                            <a href="https://www.facebook.com/UPDDChE" target="_blank" className="email-details">
                                            {ReactHtmlParser(data.facebook)}
                                            </a>
                                        </div>
                                    </div>
                                )
                            }) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                        }
                    </div>
                </div>
                <div className="footer-link hide-on-med-and-down">
                    <div className="wrapper">
                        <ul>
                            <li><Link to={"/"}>Home</Link></li>
                            <li>
                                <Link>About Us</Link>
                                <ul className="lower-link">
                                    <li><Link to={"/deanswelcome"}>Chair's Message</Link></li>
                                    <li><Link to={"/visionandmission"}>Vision and Mission</Link></li>
                                    <li><Link to={"/governingboard"}>Administration</Link></li>
                                </ul>
                            </li>
                            <li>
                                <Link>Our People</Link>
                                <ul className="lower-link">
                                    <li><Link to={"/faculty"}>Faculty</Link></li>
                                    <li><Link to={"/students"}>Students</Link></li>
                                    <li><Link to={"/alumni"}>Alumni</Link></li>
                                </ul>
                            </li>
                            <li>
                                <Link>Programs</Link>
                                <ul className="lower-link">
                                    <li><Link to={"/programsBSCHE"}>BS ChE</Link></li>
                                    <li><Link to={"/programsMSCHE"}>MS ChE</Link></li>
                                    <li><Link to={"/programsPHDCHE"}>PhD ChE</Link></li>
                                </ul>
                            </li>
                            <li>
                                <Link>Research</Link>
                                <ul className="lower-link">
                                    <li><Link to={"/researchareas"}>Research Areas</Link></li>
                                    <li><Link to={"/researchlab"}>Research Laboratories</Link></li>
                                    <li><Link to={"/publications"}>Publications</Link></li>
                                </ul>
                            </li>
                            <li><Link to={"/newsandevents"}>News And Events</Link></li>

                            <li><Link to={"/services"}>Services</Link></li>
                            <li><Link to={"/contactus"}>Contact Us</Link></li>
                        </ul>
                    </div>
                </div>
                <div className="footer-copyright">
                    <p>Copyright &copy; 2018 Department of Chemical Engineering, UP Diliman </p>
                </div>
                <div className="row">
                    <div className="nav-upper-color-1"></div>
                    <div className="nav-upper-color-2"></div>
                    <div className="nav-upper-color-3"></div>
                    <div className="nav-upper-color-4"></div>
                    <div className="nav-upper-color-5"></div>
                    <div className="nav-upper-color-6"></div>
                </div>
           </div>
       </div>
    );
}