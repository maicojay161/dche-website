import React from "react"
import MaintenanceImage from "@assets/img/maintenance.png"
import "./style"

const UnderMaintenance = () => {
    return(
        <div className="maintenance-container">
            <div className="caption">
                <h1>Under Maintenance</h1>
                <p>Unfortunately this page is under maintenance right now.</p>
            </div>
            <div className="image-container">
                <img src={MaintenanceImage}/>
            </div>
        </div>
    )
}

export default UnderMaintenance