import React, {useState, useEffect} from "react";
import axios from "axios";
import Slider from "react-slick";
import StudentsImage1 from "@assets/img/students-stories1.png"
import StudentsImage2 from "@assets/img/students-stories2.png"
import Dchelogo from "@assets/img/dchelogo.png"
import Book2 from "@assets/img/book2.JPG"
import Book3 from "@assets/img/book3.JPG"
import { Link } from "react-router-dom";
import style from "@screens/news/style.scss"
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import moment from "moment";


export default function News() {

    const [newsAndEvents, setNewsAndEvents] = useState([])
    const [articles, setArticles] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(()=>{
        axios.get(`/news-and-events`)
        .then((response) => {
            setNewsAndEvents(response.data.data.newsAndEvents)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/articles?orderBy=created_at&orderDir=DESC`)
        .then((response) => {
            setArticles(response.data.data.articles)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    },[])

    const abstractNewsEvents = abstractTitles.find((gg) => { return gg.name == 'NewsAndEvents'})
    const abstractArticles = abstractTitles.find((gg) => { return gg.name == 'NewsAndEventsArticles'})

      
    return (  
        <div className="news-main-content">

            <div className="pub-stories">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractNewsEvents ? (
                            <>
                                <h1 className="title">{abstractNewsEvents.title}</h1>
                                <p className="sub-title">{abstractNewsEvents.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="pub-stories-data">
                            <div className="pub-stories-data-content">
                                <div className="pub-stories-group">
                                    {newsAndEvents.map(newsAndEvent => (
                                         <div className="data">
                                            <div className="data-intro">
                                                <img src={newsAndEvent.image} />
                                                <div className="details">
                                                    <div className="detailsTag">
                                                        <p>{newsAndEvent.tag}</p>
                                                    </div>
                                                    <h2 className="title">{newsAndEvent.title}</h2>
                                                    <p className="excerpt">{newsAndEvent.data}</p>
                                                    <div className="extra-details">
                                                        <p className="date">Date: <span className="dateTime">{moment(newsAndEvent.dateTime).format('LL')}</span></p>
                                                        <p className="date">Location: <span className="dateTime">{newsAndEvent.location}</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                                {/* <div className="pub-stories-group">
                                    <div className="data">
                                        <div className="data-intro">
                                            <img src={Dchelogo} />
                                            <div className="details">
                                                <h2 className="title">Lorem ipsum dolor sit amet</h2>
                                                <p className="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                </p>
                                                <div className="extra-details">
                                                    <span className="date">Date: August 31, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                                {/* <Link>Read More</Link> */}
                                
                            </div>
                        </div>
                        
                    </div>
                    <div className="container articles">
                        {
                            abstractArticles ? (
                            <>
                                <h1 className="title">{abstractArticles.title}</h1>
                                <p className="sub-title">{abstractArticles.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="pub-new-stories-data">  
                            {   
                                articles.length != 0 ?
                                articles.map((article) => (
                                    <>
                                        {article.page === 'newsandevents' ? (
                                            <div className="pub-stories-data-content" id={article.id}>
                                                <img src={article.image} />
                                                <div className="data">
                                                    <h1 className="title">{article.title}</h1>
                                                    <div className="abstract">
                                                    {
                                                        ReactHtmlParser(article.abstract)
                                                    }
                                                    </div>
                                                    <a href={article.link} target="_blank">{article.link}</a>
                                                </div>
                                            </div>
                                        ) : (<></>)}
                                    </>
                                )) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
