import React, { useState, useEffect } from "react";
import axios from 'axios'
import PHDCurriculum from "@assets/img/programs/bschecurriculum.png"
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import style from "@screens/programs/bsche/style.scss"

export default function ProgramsBSCHE() {

    const [programs, setPrograms] = useState([])

    useEffect(() => {
        axios.get(`/programs`)
        .then((response) => {

            const setData = response.data.data.programs

            setData.forEach((data) => {
                if(data.id === '65cafeab-eda5-4434-aa7f-89c571a2cb76'){
                    setPrograms(data)
                }
            })
            
        }).catch((error) => {
            console.log(error)
        })
    }, [])


    return (  
        <div className="programsBSCHE-main-content">
           <div className="programsBSCHE-content">
                <div className="wrapper">
                    {
                        (programs).length != 0 ? (
                            <div className="container">
                                <div className="vision">
                                    <h2>Overview</h2>
                                    <p>{ReactHtmlParser(programs.overview)}</p>
                                </div>
                                <div className="curriculum">
                                    <h2>Curriculum</h2>
                                    <p>{ReactHtmlParser(programs.curriculum)}</p>
                                    { 
                                        [programs.images].map((data) => {
                                            return(
                                                <img src={data} />
                                            )
                                        })
                                    }
                                    { 
                                        [programs.units].map((data) => {
                                            return(
                                                <p>{ReactHtmlParser(data)}</p>
                                            )
                                        })
                                    }
                                </div>
                                <div className="mission">
                                    <h2>Admissions Criteria</h2>
                                    <p>{ReactHtmlParser(programs.admissionCriteria)}</p>
                                </div>
                            </div>
                        ) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                    }
                </div>
           </div>
        </div>
    );
}
