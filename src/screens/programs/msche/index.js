import React, { useState, useEffect }  from "react";
import MSCurriculum1 from "@assets/img/programs/mschecurriculum1.png"
import MSCurriculum2 from "@assets/img/programs/mschecurriculum2.png"
import axios from 'axios'
import PHDCurriculum from "@assets/img/programs/bschecurriculum.png"
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import style from "@screens/programs/msche/style.scss"

export default function ProgramsMSCHE() {

    const [programs, setPrograms] = useState([])

    useEffect(() => {
        axios.get(`/programs`)
        .then((response) => {
            const setData = response.data.data.programs

            setData.forEach((data) => {
                if(data.id === 'a3369ceb-9a7f-4073-8ec1-05b3255c9bd4'){
                    setPrograms(data)
                }
            })

        }).catch((error) => {
            console.log(error)
        })
    }, [])

    console.log(programs)

    return (  
        <div className="programsMSCHE-main-content">
           <div className="programsMSCHE-content">
                <div className="wrapper">
                {
                    (programs).length != 0 ? (
                        <div className="container">
                            <div className="vision">
                                <h2>Overview</h2>
                                <p>{ReactHtmlParser(programs.overview)}</p>
                            </div>
                            <div className="curriculum">
                                <h2>Curriculum</h2>
                                <p>{ReactHtmlParser(programs.curriculum)}</p>
                                { 
                                    [programs.images].map((data) => {
                                        return(
                                            <img src={data} />
                                        )
                                    })
                                }
                                { 
                                    [programs.units].map((data) => {
                                        return(
                                            <p>{ReactHtmlParser(data)}</p>
                                        )
                                    })
                                }
                            </div>
                            <div className="mission">
                                <h2>Admissions</h2>
                                <p>{ReactHtmlParser(programs.admissionCriteria)}</p>
                            </div>
                            <div className="mission">
                                <h2>Useful Links</h2>
                                {ReactHtmlParser(programs.links)}
                            </div>
                        </div>
                    ) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                }
                </div>
           </div>
        </div>
    );
}
