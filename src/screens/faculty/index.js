import React, {useState, useEffect} from "react";
import { Link, useHistory } from "react-router-dom";
import { proflist, assioproflist, assistantprof, instructors, teachingassio, adjunctprof, seniorlec, lecturer } from "./content.json";
import axios from "axios";
import { Rings } from "react-loader-spinner";
import style from "@screens/faculty/style.scss"


export default function Faculty() {

    let history = useHistory();
    const [faculty, setFaculty] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(() => {
        axios.get(`/faculty-details`)
        .then((response) => {
            setFaculty(response.data.data.faculties)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    }, [])

    function groupArrayOfObjects(list, key) {
        return list.reduce(function(rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
    };

    //sort alphabetically
    const sortedFaculty = faculty.sort((a,b) => a.name.localeCompare(b.name))

    //get data by position proffessors
    const grouped = groupArrayOfObjects(sortedFaculty,"position");

    //filtered data
    const profData = grouped.Professors
    const assProfData = grouped.AssociateProfessors
    const assisProfData = grouped.AssistantProfessors
    const instData = grouped.Instructors
    const teachAssData = grouped.TeachingAssociate
    const adjProfData = grouped.AdjunctProfessor
    const professorialLecturer = grouped.ProfessorialLecturer
    const senLecData = grouped.SeniorLecturers
    const lecData = grouped.Lecturers

    const abstractFaculty = abstractTitles.find((gg) => { return gg.name == 'Faculty'})

      
    return (  
        <div className="faculty-main-content">

            <div className="prof">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractFaculty ? (
                            <>
                                <h1 className="title">{abstractFaculty.title}</h1>
                                <p className="sub-title">{abstractFaculty.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                       
                    </div>
                </div>
            </div>

            {
                profData ? 
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Professors</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            profData ? 
                                            (<>
                                                {profData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)
            }
            
            {
                assProfData ? 
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Associate Professors</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            assProfData ? 
                                            (<>
                                                {assProfData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)
            }
            
            {
                assisProfData ? 
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Assistant Professors</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            assisProfData ? 
                                            (<>
                                                {assisProfData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)
            }
            
            {
                instData ?
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Instructors</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            instData ? 
                                            (<>
                                                {instData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)
            }
            
            {
                teachAssData ?
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Teaching Associate</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            teachAssData ? 
                                            (<>
                                                {teachAssData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)
            }
            
            {
                adjProfData ?
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Adjunct Professor</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            adjProfData ? 
                                            (<>
                                                {adjProfData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)
            }
            
            
            {
                professorialLecturer ? 
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Professorial Lecturers</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            professorialLecturer ? 
                                            (<>
                                                {professorialLecturer.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ): (<></>)
            }
            
            {
                senLecData ? 
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Senior Lecturers</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            senLecData ? 
                                            (<>
                                                {senLecData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)  
            }
            
            {
                lecData ? 
                (
                    <div className="prof">
                        <div className="wrapper">
                            <div className="container">
                                <h1 className="title">Lecturers</h1>
                                <p className="sub-title"></p>
                                <div className="prof-data">
                                    <div className="prof-data-content">
                                        {
                                            lecData ? 
                                            (<>
                                                {lecData.map(( prof) => (
                                                    <div className="prof-group" key={prof.id}>
                                                        <div className="data">
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                        </div>
                                                        <div className="data">
                                                            <span className="name">
                                                                {
                                                                    (prof.adminRoles).map((data)=>(
                                                                        <>{data}</>
                                                                    ))
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                ))}
                                            </>) : 
                                            (<><Rings color="#64001e" height={80} width={80}/></>)
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (<></>)
            }
            


            {/* <div className="prof">
                <div className="wrapper">
                    <div className="container">
                        <h1 className="title">Instructors</h1>
                        <p className="sub-title"></p>
                        <div className="prof-data">
                            <div className="prof-data-content">
                                {
                                    instData ? 
                                    (<>
                                        {instData.map(( prof) => (
                                            <div className="prof-group" key={prof.id}>
                                                <div className="data">
                                                    <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}><span className="name">{prof.name}</span></Link>
                                                </div>
                                                <div className="data">
                                                    <span className="name">
                                                        {
                                                            (prof.adminRoles).map((data)=>(
                                                                <>{data}</>
                                                            ))
                                                        }
                                                    </span>
                                                </div>
                                            </div>
                                        ))}
                                    </>) : 
                                    (<><Rings color="#64001e" height={80} width={80}/></>)
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="prof">
                <div className="wrapper">
                    <div className="container">
                        <h1 className="title">Instructors</h1>
                        <p className="sub-title">&nbsp;</p>
                        <div className="prof-data">
                            <div className="prof-data-content">
                                
                                {instructors.map(( prof, index) => (
                                    <div className="prof-group" key={index}>
                                        <div className="data">
                                            <Link to={prof.link}><span className="name">{prof.name}</span></Link>
                                        </div>
                                        <div className="data">
                                            <span className="name">{prof.jobTitle}</span>
                                        </div>
                                    </div>
                                ))}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="prof">
                <div className="wrapper">
                    <div className="container">
                        <h1 className="title">Teaching Associate</h1>
                        <p className="sub-title">&nbsp;</p>
                        <div className="prof-data">
                            <div className="prof-data-content">
                                
                                {teachingassio.map(( prof, index) => (
                                    <div className="prof-group" key={index}>
                                        <div className="data">
                                            <Link to={prof.link}><span className="name">{prof.name}</span></Link>
                                        </div>
                                        <div className="data">
                                            <span className="name">{prof.jobTitle}</span>
                                        </div>
                                    </div>
                                ))}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="prof">
                <div className="wrapper">
                    <div className="container">
                        <h1 className="title">Adjunct Professor</h1>
                        <p className="sub-title">&nbsp;</p>
                        <div className="prof-data">
                            <div className="prof-data-content">
                                
                                {adjunctprof.map(( prof, index) => (
                                    <div className="prof-group" key={index}>
                                        <div className="data">
                                            <Link to={prof.link}><span className="name">{prof.name}</span></Link>
                                        </div>
                                        <div className="data">
                                            <span className="name">{prof.jobTitle}</span>
                                        </div>
                                    </div>
                                ))}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="prof">
                <div className="wrapper">
                    <div className="container">
                        <h1 className="title">Senior Lecturers</h1>
                        <p className="sub-title">&nbsp;</p>
                        <div className="prof-data">
                            <div className="prof-data-content">
                                
                                {seniorlec.map(( prof, index) => (
                                    <div className="prof-group" key={index}>
                                        <div className="data">
                                            <Link to={prof.link}><span className="name">{prof.name}</span></Link>
                                        </div>
                                        <div className="data">
                                            <span className="name">{prof.jobTitle}</span>
                                        </div>
                                    </div>
                                ))}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="prof">
                <div className="wrapper">
                    <div className="container">
                        <h1 className="title">Lecturers</h1>
                        <p className="sub-title">&nbsp;</p>
                        <div className="prof-data">
                            <div className="prof-data-content">
                                
                                {lecturer.map(( prof, index) => (
                                    <div className="prof-group" key={index}>
                                        <div className="data">
                                            <Link to={prof.link}><span className="name">{prof.name}</span></Link>
                                        </div>
                                        <div className="data">
                                            <span className="name">{prof.jobTitle}</span>
                                        </div>
                                    </div>
                                ))}

                            </div>
                        </div>
                    </div>
                </div>
            </div> */}

        </div>
    );
}
