import React, { useState, useEffect } from "react";
import Prof from "@assets/img/prof.jpg"
import axios from "axios";
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import style from "@screens/deanswelcome/style.scss"

export default function DeansWelcome() {

    const [chairData, setChairData] = useState([])

    useEffect(() => {
        axios.get(`/chair-messages`)
        .then((response) => {
            setChairData(response.data.data.chairMessages)
        }).catch((error) => {
            console.log(error)
        })
    }, [])


    return (  
        <div className="deanswelcome-main-content">
        
           <div className="deanswelcome-content">
                <div className="wrapper">
                    <div className="container">
                        <div className="content-title">
                            <h1>Chair's Message</h1>
                        </div>
                        { 
                            chairData != 0 ? 
                            chairData.map((data, index) => {
                                return(
                                    <div className="main-content" key={index}>
                                        <div className="left">
                                            <img src={data.image} />
                                            <div className="sub">
                                                <p className="name">
                                                    {data.name}
                                                </p>
                                                <p className="job-title">
                                                    {data.position}
                                                </p>
                                            </div>
                                        </div>
                                        <div className="right">
                                            <p>  {ReactHtmlParser(data.data)}</p>
                                        </div>
                                    </div>
                                )
                            }) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                        }
                    </div>
                </div>
           </div>
        </div>
    );
}
