import React, {useState, useEffect} from "react";
import Slider from "react-slick";
import FacultyBanner from "@assets/img/faculty-banner.JPG"
import StudentImage from "@assets/img/student-home.png"
import FacultyImage from "@assets/img/faculty-img.png"
import StudentsImage1 from "@assets/img/students-stories1.png"
import StudentsImage2 from "@assets/img/students-stories2.png"
import { Link, useLocation } from "react-router-dom";
import { Rings } from "react-loader-spinner";
import ReactHtmlParser from 'react-html-parser'
import axios from "axios";
import style from "@screens/alumni/style.scss"


export default function Alumni() {

    const [contentID, setContentID] = useState()
    const [hide, sethide] = useState(true)
    const [alumnis, setAlumnis] = useState([])
    const [articles, setArticles] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(() => {
        axios.get(`/alumnis`)
        .then((response) => {
            setAlumnis(response.data.data.alumnis)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/articles?orderBy=created_at&orderDir=DESC`)
        .then((response) => {
            setArticles(response.data.data.articles)
        }).catch((error) => {
            console.log(error)
        })
        
        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    }, [])

    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    let query = useQuery();

    useEffect(() => {
        setContentID(query.get("id"))
    })

    function handleSubmit(e) {
        e.preventDefault();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    const abstractAlumni = abstractTitles.find((gg) => { return gg.name == 'AlumniTestimonials'})
    const abstractAlumniArticles = abstractTitles.find((gg) => { return gg.name == 'AlumniArticles'})

      
    return (  
        <div className="alumni-main-content">
            <div className="row">
                <div className="nav-upper-color-1"></div>
                <div className="nav-upper-color-2"></div>
                <div className="nav-upper-color-3"></div>
                <div className="nav-upper-color-4"></div>
                <div className="nav-upper-color-5"></div>
                <div className="nav-upper-color-6"></div>
            </div>
            <div className="alumni-class">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractAlumni ? (
                            <>
                                <h1 className="title">{abstractAlumni.title}</h1>
                                <p className="sub-title">{abstractAlumni.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="alumni-class-data">

                                {alumnis.map(( data) => {
                                    if(contentID === data.id){
                                        return  (
                                            <div className="testimonials" style={{display: hide=== true ? 'none' : ''}}>
                                               <div className="left">
                                                   <img src={data.image} />
                                                   <span className="name">{data.name}</span>
                                                   <p className="job-title">{data.batch}</p>
                                               </div>
                                               <div className="right">
                                                   <blockquote>
                                                       {data.testimonial}
                                                   </blockquote>
                                               </div>
                                           </div>
                                       ) 
                                    }
                                })}

                            
                            {alumnis.length != 0 ? 
                            (
                                <div className="alumni-class-data-content">
                                    <div className="alumni-class-group">
                                        {alumnis.map(( data, index) => {
                                            
                                            return(
                                                <div className="data" key={index} onClick={handleSubmit}>
                                                    <Link to={'/alumni?id='+data.id} onClick={()=>sethide(false)}><img src={data.image} /></Link>
                                                    <span className="name">{data.name}</span>
                                                    <p className="job-title">{data.batch}</p>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            ) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)}
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="nav-upper-color-1"></div>
                <div className="nav-upper-color-2"></div>
                <div className="nav-upper-color-3"></div>
                <div className="nav-upper-color-4"></div>
                <div className="nav-upper-color-5"></div>
                <div className="nav-upper-color-6"></div>
            </div>
            <div className="wrapper">
                <div className="container articles">
                    {
                        abstractAlumniArticles ? (
                        <>
                            <h1 className="title" id={'ArticlesArea'}>{abstractAlumniArticles.title}</h1>
                            <p className="sub-title">{abstractAlumniArticles.abstract}</p>
                        </>
                        ) : (<></>)
                    }
                    <div className="pub-new-stories-data">  
                        {   
                            articles.length != 0 ?
                            articles.map((article) => (
                                <>
                                    {article.page === 'alumni' ? (
                                        <div className="pub-stories-data-content" id={article.id}>
                                            <img src={article.image} />
                                            <div className="data">
                                                <h1 className="title">{article.title}</h1>
                                                <div className="abstract">
                                                {
                                                    ReactHtmlParser(article.abstract)
                                                }
                                                </div>
                                                <a href={article.link} target="_blank">{article.link}</a>
                                            </div>
                                        </div>
                                    ) : (<></>)}
                                </>
                                // <div className="pub-stories-data-content" id={article.id}>
                                //     <img src={article.image} />
                                //     <div className="data">
                                //         <h1 className="title">{article.title}</h1>
                                //         <div className="abstract">
                                //         {
                                //             ReactHtmlParser(article.abstract)
                                //         }
                                //         </div>
                                //         <a href={article.link} target="_blank">{article.link}</a>
                                //     </div>
                                // </div>
                            )) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}
