import React, {useState, useEffect} from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import axios from "axios";
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import { styled } from '@mui/material/styles';
import moment from 'moment'

import PubImage1 from "@assets/img/pub-image-sample.jpg"
import style from "@screens/publication/style.scss"
import data from "./data.json"
import UnderMaintenance from "../../components/molecule/UnderMaintenance";


export default function Publication() {
      
    const [publications, setPublications] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(()=> {
        axios.get(`/publications?orderBy=date&orderDir=DESC`)
        .then((response) => {
            setPublications(response.data.data.publications)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    },[])


    console.log("haha", publications)

    


    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
          backgroundColor: "#64001e",
          color: theme.palette.common.white,
          fontFamily: 'Avenir Black' ,
        },
        [`&.${tableCellClasses.body}`]: {
          fontSize: 14,
          verticalAlign: "top",
          fontFamily: 'sans-serif' ,
        },
      }));
      
      const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
          backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
          border: 0,
        },
      }));

    const abstractPublications = abstractTitles.find((gg) => { return gg.name == 'Publications'})
      
    return (  
        <div className="pub-main-content">
            <div className="pub-stories">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractPublications ? (
                            <>
                                <h1 className="title">{abstractPublications.title}</h1>
                                <p className="sub-title">{abstractPublications.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="pub-new-stories-data">
                            {publications != 0 ? (
                                <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                                    <TableHead>
                                    <TableRow>
                                        <StyledTableCell align="" width={"25%"}>Title</StyledTableCell>
                                        <StyledTableCell align="" width={"20%"}>Authors</StyledTableCell>
                                        <StyledTableCell align="" width={"25%"}>Journal Published</StyledTableCell>
                                        <StyledTableCell align="" width={"10%"}>Date</StyledTableCell>
                                        <StyledTableCell align="" width={"20%"}>DOI</StyledTableCell>
                                        {/* <StyledTableCell align="center" >Website Link</StyledTableCell> */}
                                    </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {  
                                        publications.map((publication) => (
                                            <StyledTableRow key={publication.id}>
                                                <StyledTableCell component="th" scope="row">
                                                    {ReactHtmlParser(publication.title)}
                                                </StyledTableCell>
                                                <StyledTableCell >
                                                        {publication.authors.map(author => (
                                                            <p style={{margin: 0}}>
                                                                {ReactHtmlParser(author)}
                                                            </p>
                                                        ))}
                                                </StyledTableCell>
                                                <StyledTableCell > {ReactHtmlParser(publication.journal)}</StyledTableCell>
                                                <StyledTableCell >{moment(publication.date).format("MMMM YYYY")}</StyledTableCell>
                                                <StyledTableCell ><a href={publication.doi} target="_blank">{ReactHtmlParser(publication.doi)}</a></StyledTableCell>
                                                {/* <StyledTableCell >{publication.link}</StyledTableCell> */}
                                            </StyledTableRow>
                                        ))
                                    }
                                    </TableBody>
                                </Table>
                                </TableContainer>
                                ):(<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                        
                        }
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
