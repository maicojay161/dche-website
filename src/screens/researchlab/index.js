import React, {useState, useEffect} from "react";
import { Link, useHistory } from "react-router-dom";
import style from "@screens/researchlab/style.scss"
import { Rings } from "react-loader-spinner";
import axios from "axios";
import data from "./data.json"


export default function Home() {

    const researchLaboratories = data.researchLaboratories

    let history = useHistory();
    const [researchLabs, setResearchLabs] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(() => {
        axios.get(`/research-labs`)
        .then((response) => {
            setResearchLabs(response.data.data.researchLabs)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    }, [])

    const sortedResearchLabs = researchLabs.sort((a,b) => a.laboratoryName.localeCompare(b.laboratoryName))
    const abstractResearchLabs = abstractTitles.find((gg) => { return gg.name == 'ResearchLabs'})


    return (  
        <div className="researchlab-main-content">

            <div className="researchlab">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractResearchLabs ? (
                            <>
                                <h1 className="title">{abstractResearchLabs.title}</h1>
                                <p className="sub-title">{abstractResearchLabs.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="researchlab-data">
							<div className="researchlab-group">
                                {
                                    researchLabs != 0 ?
                                    sortedResearchLabs.map((reslab) => (
                                        <div className="researchlab-data-content">
                                            <Link onClick={()=>{ history.push("/researchlab/individual",{data: reslab}) }}>
                                                <div className="logo"><img src={reslab.image} /></div>
                                                <span className="title">{reslab.laboratoryName}</span>
                                            </Link>
                                        </div>
                                    )) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                }
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
