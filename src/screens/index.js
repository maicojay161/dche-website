import React from "react";
import {
  Route,
  Switch,
  Redirect,
  useLocation,
  BrowserRouter as Router,
} from "react-router-dom";
import Header from "@molecule/Header/";
import Footer from "@molecule/Footer/";
import ScrollToTop from "@molecule/ScrollToTop";
import Home from "@screens/home/";
import VisionAndMission from "@screens/visionandmission/";
import GoverningBoard from "@screens/governingboard/";
import DeansWelcome from "@screens/deanswelcome";
import Faculty from "@screens/faculty";
import Staff from "@screens/staff";
import Student from "@screens/student";
import Alumni from "@screens/alumni";
import News from "@screens/news";
import Events from "@screens/events";
import Publication from "@screens/publication";
import FacultySolo from "@screens/solofaculty";
import Service from "@screens/service";

//CONTACT US
import ContactUs from "@screens/contactus";

//RESEARCH
import ResearchAreas from "@screens/researchareas";
import ResearchLab from "@screens/researchlab";
import ResearchLabSolo from "@screens/soloresearchlab";

//PROGRAMS
import ProgramsBSCHE from "@screens/programs/bsche";
import ProgramsMSCHE from "@screens/programs/msche";
import ProgramsPHDCHE from "@screens/programs/phdche";

import Fonts from "@fonts";
import MainTheme from "@src/theme.scss";

export default function Screen() {

  return (
    <div>
      <Router>
        <ScrollToTop />
        <Header />
        <div>
          <Route exact path={"/"} component={Home} />
          <Route
            exact
            path={"/visionandmission"}
            component={VisionAndMission}
          />
          <Route exact path={"/governingboard"} component={GoverningBoard} />
          <Route exact path={"/deanswelcome"} component={DeansWelcome} />
          <Route exact path={"/faculty"} component={Faculty} />
          <Route exact path={"/faculty/individual"} component={FacultySolo} />
          {/* <Route exact path={"/faculty/ocon"} component={FacultySolo2} />
                        <Route exact path={"/faculty/aberilla"} component={FacultySolo3} /> */}
          <Route exact path={"/staff"} component={Staff} />
          <Route exact path={"/students"} component={Student} />
          {/* <Route exact path={"/alumni"} component={Alumni} /> */}
          <Route exact path={"/alumni"} component={Alumni} />
          
          <Route exact path={"/publications"} component={Publication} />
          <Route exact path={"/newsandevents"} component={News} />
          {/* <Route exact path={"/events"} component={Events} /> */}

          {/* Contact Us */}
          <Route exact path={"/contactus"} component={ContactUs} />

          {/* Research */}
          <Route exact path={"/researchareas"} component={ResearchAreas} />
          <Route exact path={"/researchlab"} component={ResearchLab} />
          <Route
            exact
            path={"/researchlab/individual"}
            component={ResearchLabSolo}
          />

          {/* PROGRAMS */}
          <Route exact path={"/programsBSCHE"} component={ProgramsBSCHE} />
          <Route exact path={"/programsMSCHE"} component={ProgramsMSCHE} />
          <Route exact path={"/programsPHDCHE"} component={ProgramsPHDCHE} />

          <Route exact path={"/services"} component={Service} />
        </div>
        <Footer />
      </Router>
    </div>
  );
}
