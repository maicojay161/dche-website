import React, {useEffect, useState} from "react";
import Slider from "react-slick";
import FacultyBanner from "@assets/img/faculty-banner.JPG"
import StudentImage from "@assets/img/student-home.png"
import FacultyImage from "@assets/img/faculty-img.png"
import Deleon from "@assets/img/profs/deleon.png"
import Dalida from "@assets/img/profs/dalida.png"
import Tumolva from "@assets/img/profs/tumolva.png"
import Ocon from "@assets/img/profs/ocon.png"
import Aberilla from "@assets/img/profs/Aberilla.jpg"
import Pilario from "@assets/img/profs/pilario.png"
import { Link, useLocation, useHistory } from "react-router-dom"
import { Rings } from "react-loader-spinner";
import ReactHtmlParser from 'react-html-parser'
import Contents from './content.json'
import style from "@screens/soloresearchlab/style.scss"


export default function ResearchLabSolo() {

    const history = useHistory();
    const [data, setDate] = useState()

    // const data = history.location.state.data

    useEffect(()=>{
        setDate(history.location.state.data)
    },[])
      
    return (  
        <div className="researchsolo-main-content">
            {/* <h1 className="intro"><div className="wrapper">Department of Chemical Engineering</div></h1> */}
           <div className="wrapper">
                {/* {(Contents[contentID] || []).map(( data, index) => (
                    <div className="main-content">
                        <div className="solo-intro-content">
                            <img src={data.image}/>
                            <h1>{data.laboratory}</h1>
                            <span className="position">{data.introduction}</span>
                        </div>
                        <div className="solo-contact">
                            { data.contactperson === "" ? ('') : (<span>Contact Info : <u>{data.contactperson}</u></span>) }
                            { data.email === "" ? ('') : (<span>Email : <u>{data.email}</u></span>) }
                            { data.mobile === "" ? ('') : (<span>Mobile : <u>{data.mobile}</u></span>) }
                            { data.website === "" ? ('') : (<span>Website : <a href={data.website} target="_blank">{data.website}</a></span>) }
                        </div>
                        <div className="solo-education">
                            <h2>Members</h2>
                            {(data.members).map(( data, index) => (
                                <li key={index}>{data}</li>
                            ))}
                        </div>
                        
                    </div>
                ))} */}
                {data ? (
                    <div className="main-content">
                    <div className="solo-intro-content">
                        <img src={data.image}/>
                        <h1>{data.laboratoryName}</h1>
                        <span className="position">{ReactHtmlParser(data.introduction)}</span>
                    </div>
                    <div className="solo-contact">
                        { data.contactPerson === "" ? ('') : (<span>Contact Info : <u>{data.contactPerson}</u></span>) }
                        { data.email === "" ? ('') : (<span>Email : <u>{data.email}</u></span>) }
                        { data.mobile === "" ? ('') : (<span>Mobile : <u>{data.mobile}</u></span>) }
                        { data.website === "" ? ('') : (<span>Website : <a href={data.website} target="_blank">{data.website}</a></span>) }
                    </div>
                    <div className="solo-education">
                        <h2>Members</h2>
                        {(data.members).map(( data, index) => (
                            <li key={index}>{data}</li>
                        ))}
                    </div>
                </div>
                ):(
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>
                )}
           </div>
        </div>
    );
}
