import React, { useEffect, useState } from "react";
import axios from 'axios'
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import style from "@screens/contactus/style.scss"

export default function ContactUs() {

    const [contactUs, setContactUs] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(() => {
        axios.get(`/contact-us-details`)
        .then((response) => {
            setContactUs(response.data.data.contactUsDetails)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    const abstractContact = abstractTitles.find((gg) => { return gg.name == 'ContactUs'})


    return (  
        <div className="contactus-main-content">
           <div className="contactus-content">
                <div className="wrapper">
                    <div className="container">
                        <div className="vision">
                            {
                                abstractContact ? (
                                <>
                                    <h1 className="title">{abstractContact.title}</h1>
                                    <p className="sub-title">{abstractContact.abstract}</p>
                                </>
                                ) : (<></>)
                            }
                            {
                                contactUs.length != 0 ? 
                                contactUs.map((data,index) => {
                                    return(
                                        <div key={index}>
                                            <p>
                                                <span>Department of Chemical Engineering</span>
                                                {ReactHtmlParser(data.address)}
                                            </p>
                                            <p>
                                                <span>Telephone</span>
                                                {ReactHtmlParser(data.phone)}
                                            </p>
                                            <p>
                                                <span>Facebook</span>
                                                {ReactHtmlParser(data.facebook)}
                                            </p>
                                        </div>  
                                    )
                                }) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                            }
                           
                        </div>
                    </div>
                </div>
           </div>
        </div>
    );
}
