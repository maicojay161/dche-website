import React, {useEffect, useState} from "react";
import Slider from "react-slick";
import { ChevronRightIcon, ChevronLeftIcon } from "@icons";
import Banner1 from "@assets/img/banner/banners01.png"
import Banner2 from "@assets/img/banner/banners02.png"
import Banner3 from "@assets/img/banner/banners03.png"
import Banner4 from "@assets/img/banner/banners04.png"
import RankImage1 from "@assets/img/Rankings-01.png"
import RankImage2 from "@assets/img/Rankings-02.png"
import RankImage3 from "@assets/img/Rankings-03.png"
import ResAreas1 from "@assets/img/researchareas/data_science_maroon.png"
import ResAreas2 from "@assets/img/researchareas/drug_dev_maroon.png"
import ResAreas3 from "@assets/img/researchareas/energy_maroon.png"
import ResAreas4 from "@assets/img/researchareas/environment_maroon.png"
import ResAreas5 from "@assets/img/researchareas/industrial_maroon.png"
import ResAreas6 from "@assets/img/researchareas/manufacturing_maroon.png"
import { Link, useHistory } from "react-router-dom";
import { HashLink } from 'react-router-hash-link';
import axios from 'axios'
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import style from "@screens/home/style.scss"
import data from "./data.json"
import moment from "moment";

export default function Home() {
    let history = useHistory();

    function NextArrow({ className, onClick }) {
        return (
            <ChevronRightIcon onClick={onClick} className={className}/>
        );
    }
    function PrevArrow({ className, onClick }) {
        return (
            <ChevronLeftIcon onClick={onClick} className={className}/>
        );
    }

    const bannerSettings = {
        dots: false,
        infinite: true,
        autoplay: true,
        lazyload: true,
        pauseOnHover: false,
        autoplaySpeed: 6000,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <NextArrow className="next"/>,
        prevArrow: <PrevArrow className="prev" />
    };

    const testimonialSettings = {
        dots: true,
        infinite: true,
        autoplay: true,
        lazyload: true,
        pauseOnHover: false,
        autoplaySpeed: 3000,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };
    


    const [accordFirst, setAccordFirst] = useState(false)
    const [accordSecond, setAccordSecond] = useState(false)
    const [accordThird, setAccordThird] = useState(true)
    const [homeSliders, setHomeSliders] = useState([])
    const [researchAreas, setResearchAreas] = useState([])
    const [researchLabs, setResearchLabs] = useState([])
    const [faculty, setFaculty] = useState([])
    const [newsAndEvents, setNewsAndEvents] = useState([])
    const [students, setStudents] = useState([])

    const [bsPrograms, setBSPrograms] = useState([])
    const [msPrograms, setMSPrograms] = useState([])
    const [phdPrograms, setPHDPrograms] = useState([])
    const [homeRankings, setHomeRankings] = useState([])
    const [homeGlance, setHomeGlance] = useState([])
    const [articles, setArticles] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

   
 

    useEffect(() => {

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/programs`)
        .then((response) => {
            
            const setData = response.data.data.programs

            setData.forEach((data) => {
                if(data.id === 'a3369ceb-9a7f-4073-8ec1-05b3255c9bd4'){
                    setMSPrograms(data)
                }else if(data.id === '8fe76449-0e8c-4f5f-b810-e5de616c895f'){
                    setPHDPrograms(data)
                }else{
                    setBSPrograms(data)
                }
            })

        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/home-sliders`)
        .then((response) => {
            setHomeSliders(response.data.data.homeSliders)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/research-areas?perpage=6`)
        .then((response) => {
            setResearchAreas(response.data.data.researchAreas)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/research-labs`)
        .then((response) => {
            setResearchLabs(response.data.data.researchLabs)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/faculty-details`)
        .then((response) => {
            setFaculty(response.data.data.faculties)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/news-and-events?perPage=3`)
        .then((response) => {
            setNewsAndEvents(response.data.data.newsAndEvents)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/home-rankings`)
        .then((response) => {
            setHomeRankings(response.data.data.homeRankings)
        }).catch((error) => {
            console.log(error)
        })
        
        axios.get(`/home-student-glances`)
        .then((response) => {
            setHomeGlance(response.data.data.homeStudentGlances)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/students?perPage=3`)
        .then((response) => {
            setStudents(response.data.data.students)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/articles?orderBy=created_at&orderDir=DESC`)
        .then((response) => {
            setArticles(response.data.data.articles)
        }).catch((error) => {
            console.log(error)
        })

        
    },[])

    function groupArrayOfObjects(list, key) {
        return list.reduce(function(rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
    };

    //sort alphabetically
    const sortedFaculty = faculty.sort((a,b) => a.name.localeCompare(b.name))
    const sortedResearchLabs = researchLabs.sort((a,b) => a.laboratoryName.localeCompare(b.laboratoryName))
    const sortedResearchAreas = researchAreas.sort((a,b) => a.title.localeCompare(b.title))

    //get data by name abstract title
    const abstractRanking = abstractTitles.find((gg) => { return gg.name == 'HomeRanking'})
    const abstractEvents = abstractTitles.find((gg) => { return gg.name == 'HomeEvents'})
    const abstractOurStudents = abstractTitles.find((gg) => { return gg.name == 'HomeOurStudents'})
    const abstractFaculty = abstractTitles.find((gg) => { return gg.name == 'HomeFaculty'})
    const abstractPrograms = abstractTitles.find((gg) => { return gg.name == 'HomePrograms'})
    const abstractResearchAreas = abstractTitles.find((gg) => { return gg.name == 'HomeResearchAreas'})
    const abstractResearchLab = abstractTitles.find((gg) => { return gg.name == 'HomeResearchLaboratories'})
    const abstractArticles = abstractTitles.find((gg) => { return gg.name == 'HomeArticles'})

    function onClickAccord1(){
        if( accordThird ){
            setAccordFirst(false)
            setAccordSecond(true)
            setAccordThird(false)
        }else {
            setAccordFirst(false)
            setAccordSecond(false)
            setAccordThird(true)
        }
    }

    function onClickAccord2(){
        if( accordSecond ){
            setAccordFirst(true)
            setAccordSecond(false)
            setAccordThird(false)
        }else {
            setAccordFirst(false)
            setAccordSecond(true)
            setAccordThird(false)
        }
    }

    function onClickAccord3(){
        if( accordFirst ){
            setAccordFirst(false)
            setAccordSecond(false)
            setAccordThird(true)
        }else {
            setAccordFirst(true)
            setAccordSecond(false)
            setAccordThird(false)
        }
    }

    // const events = data.events
    // const studentTestimonials = data.studentTestimonials
    // const facultyMembers = data.facultyMembers
    // const researchLaboratories = data.researchLaboratories
    // const publications = data.publications

    return (  
        <div className="home-main-content">
             <div className="banner">
                 {homeSliders.length != 0 ? 
                    <Slider {...bannerSettings}>
                    {
                        homeSliders.map((homeSlider) => (
                            <div key={homeSlider.id} className="slider-content">
                                <img src={homeSlider.image} />
                            </div>
                        ))
                        
                    }
                    </Slider>
                 : (
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>
                )}
                
            </div>
            
            <div className="rankings">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractRanking ? (
                            <>
                                <h1 className="title">{abstractRanking.title}</h1>
                                <p className="sub-title">{abstractRanking.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="rankings-data">
                            {
                                homeRankings.length != 0 ? 
                                homeRankings.map((data,index) => {
                                    return(
                                        <div className="row-data" key={index}>
                                            <img src={data.image} />
                                            <div className="ranks">
                                                <span className="actual-rank">{data.rank}</span>
                                                <span className="sub-rank">{data.subtitle}</span>
                                                <span className="sub-data">{data.data}</span>
                                            </div>
                                        </div>
                                    )
                                }) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                            }
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="eventsandtestimonials">
                <div className="wrapper">
                    <div className="container">
                        <div className="content">
                            <div className="events">abstractEvents
                                {
                                    abstractEvents ? (
                                    <>
                                        <h1 className="title">{abstractEvents.title}</h1>
                                        <p className="sub">{abstractEvents.abstract}</p>
                                    </>
                                    ) : (<></>)
                                }
                             
                                <div className="events-data">
                                    {newsAndEvents.length != 0 ? 
                                        newsAndEvents.map((newsAndEvent, i) => (
                                            <div className="events-details" key={i}>
                                                <div className="left">
                                                    <span className="day">{moment(newsAndEvent.dateTime).format('D')}</span>
                                                    <span className="month">{moment(newsAndEvent.dateTime).format('MMMM')}</span>
                                                    <span className="year">{moment(newsAndEvent.dateTime).format('YYYY')}</span>
                                                </div>
                                                <div className="right">
                                                    <p className="event-title">{newsAndEvent.title}</p>
                                                    {newsAndEvent.time && <p className="time"><span>Time: </span>{moment(newsAndEvent.dateTime).format('LL')}</p>}
                                                    {newsAndEvent.location && <p className="location"><span>Location: </span>{newsAndEvent.location}</p>}
                                                </div>
                                            </div>
                                        ))
                                        : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                    }
                                    {/* <Link to={"/events/"}>View All</Link> */}
                                </div>
                                
                            </div>
                            <div className="testimonials">
                                <Slider {...testimonialSettings}>
                                    {
                                        students.map((student)=> (
                                            <div className="testimonials-content">
                                                <img src={student.image}/>
                                                <h2 className="name">{student.name}</h2>
                                                <p className="desc">{student.batch}</p>
                                                <blockquote>
                                                   {student.testimonial.length > 200 ? student.testimonial.substring(0,200) + '...' : student.testimonial}
                                                </blockquote>
                                                <Link to={'/students'}>Learn More</Link>
                                            </div>
                                        ))
                                    }
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="ourstudents">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractOurStudents ? (
                            <>
                                <h1 className="title">{abstractOurStudents.title}</h1>
                                <p className="sub-title">{abstractOurStudents.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        {
                            homeGlance.length != 0 ? 
                            homeGlance.map((data,index) => {
                                return(
                                    <div className="ourstudents-data" key={index}>
                                        <div className="content">
                                            <span className="intake">{data.label} </span>
                                            <div className="middle">
                                                <div className="header">
                                                    <h2>{data.studentData}</h2>
                                                    <p className="sub-text">{data.studentTag}</p>
                                                </div>
                                                <p>{data.studentContent}</p>
                                            </div>
                                            <div className="last">
                                                <div className="header">
                                                    <h2 className="header">{data.slotData}</h2>
                                                    <p className="sub-text">{data.slotTag}</p>
                                                </div>
                                                <p>{data.slotContent}</p>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                        }
                        <Link to={"/students/"}>Learn More</Link>
                    </div>
                </div>
            </div>

			<div className="faculty">
                <div className="wrapper">
                    <div className="container">
                        <div className="content">
                            <div className="faculty-data">
                                {
                                    abstractFaculty ? (
                                    <>
                                        <h1 className="title">{abstractFaculty.title}</h1>
                                        <p className="sub">{abstractFaculty.abstract}</p>
                                    </>
                                    ) : (<></>)
                                }
                                <div className="faculty-data-content">
                                    <div className="faculty-group">
                                        {faculty && faculty.length != 0 ? 
                                            sortedFaculty.map((prof) => (
                                                <>
                                                    {prof.featured === true ? (
                                                         <div className="data">
                                                            <img src={prof.image} />
                                                            <Link onClick={()=>{ history.push("/faculty/individual",{data: prof}) }}>
                                                                <span className="name">{prof.name}</span>
                                                            </Link>
                                                            {
                                                                prof.adminRoles.map((postition) => (
                                                                    <p className="job-title">{postition}</p>
                                                                ))
                                                            }
                                                        </div>
                                                    ) : (<></>)}
                                                </>
                                            ))
                                            :
                                            (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                        }
                                    </div>
                                    <Link to={"/faculty/"} className="readmore">View All</Link>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="mastersandphd">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractPrograms ? (
                            <>
                                <h1 className="title">{abstractPrograms.title}</h1>
                                <p className="sub-title">{abstractPrograms.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="accordion-wrapper">
                            <div className="phd-header" id="ah1" onClick={onClickAccord3}><h2>PhDChE</h2></div>
                            <div className={"phd-body"+" "+(accordFirst ? "active" : "")}  id="ac1">
                                <div className="body-content">
                                    <div className="phd-data" 
                                        style={{ 
                                            visibility: accordFirst ? "visible" : "hidden", 
                                            opacity: accordFirst ? 1 : 0 ,
                                            transition:  accordFirst ? "visibility 1s, opacity 1s ease-out" : "visibility 0s, opacity 0s linear"
                                        }}>
                                        <div className="phd-data-content">
                                           <div className="intro">
                                                <h2>{phdPrograms.title}</h2>
                                                <p className="sub">{phdPrograms.subtitle}</p>
                                           </div>
                                           <div className="box">
                                               <div className="box-data-group">
                                                    <div className="box-data">
                                                        <span className="title">Class Profile</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(phdPrograms.classProfile)}
                                                        </p>
                                                    </div>
                                                    <div className="box-data">
                                                        <span className="title">Curriculum</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(phdPrograms.curriculum)}
                                                        </p>
                                                    </div>
                                               </div>
                                               <div className="box-data-group">
                                                    <div className="box-data">
                                                        <span className="title">Admissions Criteria</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(phdPrograms.admissionCriteria)}
                                                        </p>
                                                    </div>
                                                    <div className="box-data">
                                                        <span className="title">Faculty</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(phdPrograms.faculty_detail)}
                                                        </p>
                                                    </div>
                                               </div>
                                               <Link to={"/programsPHDCHE"}>Read More</Link>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="masters-header" id="ah2" onClick={onClickAccord2}><h2>MSChE</h2></div>
                            <div className={"masters-body"+" "+(accordSecond ? "active" : "")}  id="ac2">
                                <div className="body-content" style={{ display: accordSecond ? "block" : "none" }}>
                                    <div className="masters-data" 
                                        style={{ 
                                            visibility: accordSecond ? "visible" : "hidden", 
                                            opacity: accordSecond ? 1 : 0 ,
                                            transition:  accordSecond ? "visibility 2s, opacity 1.5s ease-out" : "visibility 0s, opacity 0s linear"
                                        }}>
                                        <div className="masters-data-content">
                                           <div className="intro">
                                                <h2>{msPrograms.title}</h2>
                                                <p className="sub">{msPrograms.subtitle}</p>
                                           </div>
                                           <div className="box">
                                               <div className="box-data-group">
                                                    <div className="box-data">
                                                        <span className="title">Class Profile</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(msPrograms.classProfile)}
                                                        </p>
                                                    </div>
                                                    <div className="box-data">
                                                        <span className="title">Curriculum</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(msPrograms.curriculum)}
                                                        </p>
                                                    </div>
                                               </div>
                                               <div className="box-data-group">
                                                    <div className="box-data">
                                                        <span className="title">Admissions</span>
                                                        <p className="desc">
                                                        {ReactHtmlParser(msPrograms.admissionCriteria)}
                                                        </p>
                                                    </div>
                                                    <div className="box-data">
                                                        <span className="title">Research Opportunities</span>
                                                        <p className="desc">
                                                        {ReactHtmlParser(msPrograms.faculty_detail)}
                                                        </p>
                                                    </div>
                                               </div>
                                               <Link to={"/programsMSCHE"}>Read More</Link>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="bs-header" id="ah2" onClick={onClickAccord1}><h2>BSChE</h2></div>
                            <div className={"bs-body"+" "+(accordThird ? "active" : "")}  id="ac2">
                                <div className="body-content" style={{ display: accordThird ? "block" : "none" }}>
                                    <div className="bs-data" 
                                        style={{ 
                                            visibility: accordThird ? "visible" : "hidden", 
                                            opacity: accordThird ? 1 : 0 ,
                                            transition:  accordThird ? "visibility 2s, opacity 1.5s ease-out" : "visibility 0s, opacity 0s linear"
                                        }}>
                                        <div className="bs-data-content">
                                           <div className="intro">
                                                <h2>{bsPrograms.title}</h2>
                                                <p className="sub">{bsPrograms.subtitle}</p>
                                           </div>
                                           <div className="box">
                                               <div className="box-data-group">
                                                    <div className="box-data">
                                                        <span className="title">Class Profile</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(bsPrograms.classProfile)}
                                                        </p>
                                                    </div>
                                                    <div className="box-data">
                                                        <span className="title">Curriculum</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(bsPrograms.curriculum)}
                                                        </p>
                                                    </div>
                                               </div>
                                               <div className="box-data-group">
                                                    <div className="box-data">
                                                        <span className="title">Admissions Criteria</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(bsPrograms.admissionCriteria)}
                                                        </p>
                                                    </div>
                                                    <div className="box-data">
                                                        <span className="title">Faculty</span>
                                                        <p className="desc">
                                                            {ReactHtmlParser(bsPrograms.faculty_detail)}
                                                        </p>
                                                    </div>
                                               </div>
                                               <Link to={"/programsBSCHE"}>Read More</Link>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="researchareas">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractResearchAreas ? (
                            <>
                                <h1 className="title">{abstractResearchAreas.title}</h1>
                                <p className="sub">{abstractResearchAreas.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="researchareas-data">
							<div className="researchareas-group">
                                {researchAreas && researchAreas.length != 0 ? 
                                    sortedResearchAreas.map(researchArea => (
                                        <div key={researchArea.id} className="researchareas-data-content">
                                            <img src={researchArea.image} />
                                            <span className="title">{researchArea.title}</span>
                                        </div>
                                    ))
                                    : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                }
							</div>
							<Link to={"/researchareas"}>View More</Link>
                        </div>
                    </div>
                </div>
            </div>

            <div className="researchlab">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractResearchLab ? (
                            <>
                                <h1 className="title">{abstractResearchLab.title}</h1>
                                <p className="sub-title">{abstractResearchLab.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="researchlab-data">
							<div className="researchlab-group">
                                {researchLabs && researchLabs.length != 0 ? 
                                    sortedResearchLabs.map((researchLab) => (
                                      <>
                                        {
                                            researchLab.featured === true ? (
                                                <div key={researchLab.id} className="researchlab-data-content">
                                                    <Link onClick={()=>{ history.push("/researchlab/individual",{data: researchLab}) }}>
                                                        <div className="logo"><img src={researchLab.image} /></div>
                                                        <span className="title">{researchLab.laboratoryName}</span>
                                                    </Link>
                                                </div>
                                            ) : (<></>)
                                        }
                                      </>
                                    ))
                                    : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                }
							</div>
							<Link to={"/researchlab"} className="readmore">View More</Link>
                        </div>
                    </div>
                </div>
            </div>

            <div className="publication">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractArticles ? (
                            <>
                                <h1 className="title">{abstractArticles.title}</h1>
                                <p className="sub-title">{abstractArticles.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="publication-data">
							<div className="publication-group">
                                {   
                                    articles.length != 0 ? 
                                    articles.slice(0, 4).map((article) => (
                                        // <>
                                        //     {article.page === 'newsandevents' ? (
                                        //         <HashLink to={'/newsandevents#'+article.id}>
                                        //             <div className="publication-data-content">
                                        //                 <div className="left">
                                        //                     <div className="left-data">
                                        //                         {/* <span className="date">{publication.date}</span> */}
                                        //                     </div>
                                        //                     <img src={article.image}></img>
                                        //                 </div>
                                        //                 <div className="right">
                                        //                     <div className="right-data">
                                        //                         <span className="journal">{article.title}</span>
                                        //                         <h1 className="title">{article.caption}</h1>
                                        //                         <p className="sub-data">{ReactHtmlParser(article.abstract.slice(0, 100)+'...')}</p>
                                        //                     </div>
                                        //                 </div>
                                        //             </div>
                                        //         </HashLink>
                                        //     ) : (<></>)}
                                        // </>
                                        <HashLink to={article.page === 'newsandevents' ? '/newsandevents#'+article.id : '/alumni#'+article.id}>
                                            <div className="publication-data-content">
                                                <div className="left">
                                                    <div className="left-data">
                                                        {/* <span className="date">{publication.date}</span> */}
                                                    </div>
                                                    <img src={article.image}></img>
                                                </div>
                                                <div className="right">
                                                    <div className="right-data">
                                                        <span className="journal">{article.title}</span>
                                                        <h1 className="title">{article.caption}</h1>
                                                        <p className="sub-data">{ReactHtmlParser(article.abstract.slice(0, 100)+'...')}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </HashLink>
                                    )) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                }
							</div>
                            <HashLink className="readmore" to={'/newsandevents#ArticlesArea'}>View More</HashLink>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    );
}
