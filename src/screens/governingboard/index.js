import React, { useEffect, useState } from "react";
import style from "@screens/governingboard/style.scss"
import { Rings } from "react-loader-spinner";
import axios from "axios";
import data from './data.json'

export default function GoverningBoard() {

    const administrative =  data.administrative

    const [committees, setComittees] = useState([])
    const [staffs, setStaffs] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(() => {
        axios.get(`/committees`)
        .then((response) => {
            setComittees(response.data.data.committees)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    }, [])

    useEffect(() => {
        axios.get(`/staffs`)
        .then((response) => {
            setStaffs(response.data.data.staffs)
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    const abstractAdministration = abstractTitles.find((gg) => { return gg.name == 'Administration'})

    return (  
        <div className="governingboard-main-content">
           <div className="governingboard-content">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractAdministration ? (
                              <>
                                <div className="content-title">
                                        <h1 className="title"> {abstractAdministration.title}</h1>
                                    </div>
                                <div className="sub-data">
                                    <p className={'thin'}>
                                            {abstractAdministration.abstract}
                                    </p>
                                        <p>ADMINISTRATIVE COMMITTEE:</p>
                                </div>
                              </>
                            ) : (<></>)
                        }
                       <div className="main-data">
                            {   
                                committees != 0 ?
                                committees.map((admin, index) => (
                                    <div key={index}>
                                        <div className="name"><p>{admin.name}</p></div>
                                        <div className="location"><p>{admin.position}</p></div>
                                    </div>
                                )) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                            }
                        </div>

                        <div className="sub-data">
                            <p>ADMINISTRATIVE STAFF:</p>
                       </div>
                       <div className="main-data">
                            {   
                                staffs != 0 ? 
                                staffs.map((admin , index) => (
                                    <div key={index}>
                                        <div className="name"><p>{admin.name}</p></div>
                                        <div className="location"><p>{admin.position}</p></div>
                                    </div>
                                )) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                            }
                        </div>
                    </div>
                </div>
           </div>
        </div>
    );
}
