import React, {useEffect, useState} from "react";
import Slider from "react-slick";
import StudentOrg1 from "@assets/img/faculty/students/alchemes.png"
import StudentOrg2 from "@assets/img/faculty/students/kem.png"
import StudentOrg3 from "@assets/img/faculty/students/chesi.png"
import { useLocation, Link } from "react-router-dom";
import axios from "axios";
import { Rings } from "react-loader-spinner";
import ReactHtmlParser from 'react-html-parser'
import Contents from './content.json'
import style from "@screens/student/style.scss"


export default function Faculty() {

    const [contentID, setContentID] = useState()
    const [hide, sethide] = useState(true)
    const [students, setStudents] = useState([])
    const [studentOrgs, setStudentOrgs] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(() => {
        axios.get(`/student-organizations`)
        .then((response) => {
            setStudentOrgs(response.data.data.studentOrganizations)
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    useEffect(() => {
        axios.get(`/students`)
        .then((response) => {
            setStudents(response.data.data.students)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    }, [])

    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    let query = useQuery();

    useEffect(() => {
        setContentID(query.get("id"))
    })

    function handleSubmit(e) {
        e.preventDefault();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    const abstractStudents = abstractTitles.find((gg) => { return gg.name == 'StudentTestimonials'})
    const abstractStudentOrgs = abstractTitles.find((gg) => { return gg.name == 'StudentOrgs'})

      
    return (  
        <div className="student-main-content">
            <div className="row">
                <div className="nav-upper-color-1"></div>
                <div className="nav-upper-color-2"></div>
                <div className="nav-upper-color-3"></div>
                <div className="nav-upper-color-4"></div>
                <div className="nav-upper-color-5"></div>
                <div className="nav-upper-color-6"></div>
            </div>
            <div className="student-class">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractStudents ? (
                            <>
                                <h1 className="title">{abstractStudents.title}</h1>
                                <p className="sub-title">{abstractStudents.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="student-class-data">

                                {students.map(( data) => {
                                    if(contentID === data.id){
                                        return  (
                                            <div className="testimonials" style={{display: hide=== true ? 'none' : ''}}>
                                               <div className="left">
                                                   <img src={data.image} />
                                                   <span className="name">{data.name}</span>
                                                   <p className="job-title">{data.batch}</p>
                                               </div>
                                               <div className="right">
                                                   <blockquote>
                                                       {data.testimonial}
                                                   </blockquote>
                                               </div>
                                           </div>
                                       ) 
                                    }
                                })}

                            
                            {students.length != 0 ? 
                            (
                                <div className="student-class-data-content">
                                    <div className="student-class-group">
                                        {students.map(( data, index) => {
                                            
                                            return(
                                                <div className="data" key={index} onClick={handleSubmit}>
                                                    <Link to={'/students?id='+data.id} onClick={()=>sethide(false)}><img src={data.image} /></Link>
                                                    <span className="name">{data.name}</span>
                                                    <p className="job-title">{data.batch}</p>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            ) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)}
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="nav-upper-color-1"></div>
                <div className="nav-upper-color-2"></div>
                <div className="nav-upper-color-3"></div>
                <div className="nav-upper-color-4"></div>
                <div className="nav-upper-color-5"></div>
                <div className="nav-upper-color-6"></div>
            </div>
            <div className="student-stories">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractStudentOrgs ? (
                            <>
                                <h1 className="title">{abstractStudentOrgs.title}</h1>
                                <p className="sub-title">{abstractStudentOrgs.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        
                        <div className="student-stories-data">
                            <div className="student-stories-data-content">
                                <div className="student-stories-group">

                                    {
                                      studentOrgs.length!=0 ?

                                        studentOrgs.map(( data, index) => {
                                            return(
                                                <div className="data" key={index}>
                                                    <img src={data.image} />
                                                    <div className="details">
                                                        <h2 className="title">{data.title}</h2>
                                                        <p className="excerpt">{ReactHtmlParser(data.data)}</p>
                                                    </div>
                                                    <div className="lower-details">
                                                        <Link to={{pathname: data.url}} target="_blank">Learn More</Link>
                                                    </div>
                                                </div>
                                            )
                                        }) : (<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
