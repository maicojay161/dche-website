import React, { useEffect, useState } from "react";
import axios from "axios";
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import style from "@screens/visionandmission/style.scss"

export default function VisionAndMission() {

    const [missionvision, setMissionVision] = useState([])

    useEffect(() => {
        axios.get(`/mission-and-visions`)
        .then((response) => {
            setMissionVision(response.data.data.missionAndVisions)
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    return (  
        <div className="visionandmission-main-content">
           <div className="visionandmission-content">
                <div className="wrapper">
                    {
                        missionvision != 0 ?
                        missionvision.map((data,index)=>{
                           return(
                            <div className="container" key={index}>
                                <div className="vision">
                                    <h2>Vision</h2>
                                    <p>{ReactHtmlParser(data.vision)}</p>
                                </div>
                                <div className="mission">
                                    <h2>Mission</h2>
                                    <p>{ReactHtmlParser(data.mission)}</p>
                                </div>
                            </div>
                           )
                        })  : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                    }
                    
                </div>
           </div>
        </div>
    );
}
