import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactHtmlParser from 'react-html-parser'
import { Rings } from "react-loader-spinner";
import './style.scss'

const Service = () => {

    const [services, setServices] = useState([])

    useEffect(() => {
        axios.get(`/services`)
        .then((response) => {
            setServices(response.data.data.services)
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    return (
        <div className="services-main-content">
           <div className="services-content">
                <div className="wrapper">
                    {
                        services != 0 ?
                        services.map((data,index)=>{
                           return(
                            <div className="container" key={index}>
                              {ReactHtmlParser(data.data)}
                            </div>
                           )
                        })  : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                    }
                    
                </div>
           </div>
        </div>
    )
}

export default Service