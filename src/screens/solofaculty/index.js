import React, { useEffect, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";
import Contents from "./content.json";
import style from "@screens/solofaculty/style.scss";

export default function FacultySolo() {
  
  const history = useHistory();
  const data = history.location.state.data

  return (
    <div className="facultysolo-main-content">
      <div className="wrapper">
          <div className="main-content">
            <div className="solo-intro-content">
              <img src={data.image} />
              <div className="positionrole">
                <h1>{data.name}</h1>
                <span className="position">{data.positionTitle}</span>
                <span className="adminrole">{data.mainRole}</span>
                <div className="solo-contact">
                  {data.email === "" ? (
                    ""
                  ) : (
                    <span>
                      Email Address : <u>{data.email}</u>
                    </span>
                  )}
                  {data.website === "" ? (
                    ""
                  ) : (
                    <span>
                      {data.website === " " ? '' : (data.website === "" ? '' : 'Website:')}
                      <a href={data.website} target="_blank">
                        {data.website}
                      </a>
                    </span>
                  )}
                  {data.googleScholarLink === "" ? (
                    ""
                  ) : (
                    <span>
                      Google Scholar Link :{" "}
                      <a href={data.googleScholarLink} target="_blank">
                        {data.googleScholarLink}
                      </a>
                    </span>
                  )}
                </div>
              </div>
            </div>
            <div className="solo-education">
              <h2>Education</h2>
              {(data.educations).map((data, index) => (
                <li key={index}>{data}</li>
              ))}
            </div>
            <div className="lab">
              {console.log(data.laboratory)}
              {(data.laboratory).length == 0 ? (<></>) : <h2>Laboratory</h2>}
              {(data.laboratory).length == 0 ? 
                (<></>) : (<li>{data.laboratory}</li>)
              }
            </div>
            <div className="solo-research">
              {(data.researchInterests).length == 0 ? (<></>) : <h2>Research Interests/Industry Experience</h2>}
              {(data.researchInterests).map((data, index) => (
                <li key={index}>{data}</li>
              ))}
            </div>
            <div className="solo-publications">
              {(data.publications).length == 0 ? (<></>) : (data.publications[0] === "" ? (<></>) : <h2>Publications</h2>)}
              {
                (data.publications).length == 0 ? (<></>) : (data.publications[0] === "" ? <></> :
                  (data.publications).map((data, index) => (
                    <li key={index}>{data}</li>
                  ))
                )
              }
            </div>
          </div>

      </div>
    </div>
  );
}
