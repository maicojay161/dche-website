import React, {useState, useEffect} from "react";
import ResAreas1 from "@assets/img/researchareas/data_science_maroon.png"
import ResAreas2 from "@assets/img/researchareas/drug_dev_maroon.png"
import ResAreas3 from "@assets/img/researchareas/energy_maroon.png"
import ResAreas4 from "@assets/img/researchareas/environment_maroon.png"
import ResAreas5 from "@assets/img/researchareas/industrial_maroon.png"
import ResAreas6 from "@assets/img/researchareas/manufacturing_maroon.png"
import { Link, useLocation } from "react-router-dom";
import ReactHtmlParser from 'react-html-parser'
import axios from 'axios';
import { Rings } from "react-loader-spinner";
import style from "@screens/researchareas/style.scss"


export default function ResearchAreas() {

    const [contentID, setContentID] = useState()
    const [hide, sethide] = useState(true)
    const [researchAreas, setResearchAreas] = useState([])
    const [abstractTitles, setAbstractTitles] = useState([])

    useEffect(() => {
        axios.get(`/research-areas`)
        .then((response) => {
            setResearchAreas(response.data.data.researchAreas)
        }).catch((error) => {
            console.log(error)
        })

        axios.get(`/abstract-titles`)
        .then((response) => {
            setAbstractTitles(response.data.data.abstractTitles)
        }).catch((error) => {
            console.log(error)
        })

    }, [])

    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    let query = useQuery();

    useEffect(() => {
        setContentID(query.get("id"))
    })

    function handleSubmit(e) {
        console.log(contentID)
        e.preventDefault();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    const abstractResearchAreas = abstractTitles.find((gg) => { return gg.name == 'ResearchAreas'})

    return (  
        <div className="researchareas-main-content">

            <div className="researchareas">
                <div className="wrapper">
                    <div className="container">
                        {
                            abstractResearchAreas ? (
                            <>
                                <h1 className="title">{abstractResearchAreas.title}</h1>
                                <p className="sub-title">{abstractResearchAreas.abstract}</p>
                            </>
                            ) : (<></>)
                        }
                        <div className="researchareas-data">

                            {researchAreas.map(( data) => {
                                if(contentID === data.id){
                                    return  (
                                        <div className="testimonials" style={{display: hide=== true ? 'none' : ''}}>
                                            <div className="left">
                                                <img src={data.image} />
                                                {/* <span className="name">{data.title}</span> */}
                                                <p className="job-title">{data.title}</p>
                                            </div>
                                            <div className="right">
                                                <blockquote>
                                                    {ReactHtmlParser(data.description)}
                                                </blockquote>
                                            </div>
                                        </div>
                                    ) 
                                }
                            })}

							<div className="researchareas-group">

                                {   
                                    researchAreas != 0 ?
                                    researchAreas.map((data) => {
                                        return(
                                            <div className="researchareas-data-content" key={data.id} onClick={handleSubmit}>
                                                <Link to={'/researchareas?id='+data.id} onClick={()=>sethide(false)}><img src={data.image} /></Link>
                                                <span className="title">{data.title}</span>
                                            </div>
                                        )
                                    }) : (<div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Rings color="#64001e" height={80} width={80}/></div>)
                                }
								
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
