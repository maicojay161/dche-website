import React from "react";
import UnderMaintenance from "../../components/molecule/UnderMaintenance";
import style from "./style.scss"

const AlumniMaintenance = () => {
    return(
        <div className="alumni-container">
            <div className="alumni-content">
                <div className="wrapper">
                    <div className="container">
                        <h1 className="title">Alumni</h1>
                        <UnderMaintenance/>
                    </div>
                </div>
            </div>
        </div>
       
    )
}

export default AlumniMaintenance