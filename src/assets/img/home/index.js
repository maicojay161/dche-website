import featuredstudent1 from './featuredstudent1.jpg'
import featuredstudent2 from './featuredstudent2.jpg'
import featuredstudent3 from './featuredstudent3.jpg'

export {
    featuredstudent1,
    featuredstudent2,
    featuredstudent3,
}