import React from "react";
import Screen from "@screens";
import axios from "axios";

export default function App() {
  axios.defaults.baseURL = `http://10.16.12.42:8081/`;
  return <Screen />;
}
